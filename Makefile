#**********************************************************
#
#      filename:   Makefile
#
#   description:   Compiles keeplog application
#                  
#        author:   McGrail, Kevin
#      login id:   FA_18_CPS444_14
#
#         class:   CPS 444
#    instructor:   Perugini
#    assignment:   Homework #5
#
#      assigned:   September 18, 2018
#           due:   September 25, 2018
#
# **********************************************************#

CC=gcc
CC_OPTS=-c
OBJECTS=$(addsuffix .o,$(SRC))
PGM=keeplog
SRC1=$(wildcard *.c)
SRC=$(patsubst %.c,%,$(SRC1))

$(PGM): $(OBJECTS)
	$(CC) -s -o $(PGM) $(OBJECTS) -L/home/FA_18_CPS444_14/lib/ -lkeeplog_helper2 -lkeeplog_helper1 -llist

$(OBJECTS): $(PGM).c
	$(CC) $(CC_OPTS) $(PGM).c -I/home/FA_18_CPS444_14/include/

clean:
	@-rm *.o $(PGM)
